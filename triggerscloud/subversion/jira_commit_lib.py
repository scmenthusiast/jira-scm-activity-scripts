#!/usr/bin/env python3

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from json import loads, dumps
from datetime import datetime
import requests
import subprocess
import sys
import re
import tzlocal
import pytz


class ScmActivity(object):

    def __init__(self):
        """
        __init__(). scm changeset object with getters and setters
        :return: None
        """

        self.issueKey = None
        self.changeId = None
        self.changeType = None
        self.changeDate = None
        self.changeAuthor = None
        self.changeBranch = None
        self.changeStatus = None
        self.changeTag = None
        self.changeLink = None
        self.changeMessage = None
        self.changeFiles = None
        self.changeUpdate = None


class JiraCommitHook(object):

    def __init__(self, config):
        """
        __init__(). prepares or initiates configuration object
        :param config: config object
        :return: None
        """

        self.config = config
        self.jira_cloud_scm_url = config.get("jira.cloud.scm.url")

    def jira_update(self, scm_activity, issue_keys, change_update=0):
        """
        jira_update(). It updates change-set details for give issue keys on JIRA
        :param scm_activity: change set dictionary object
        :param issue_keys: change issue keys
        :param change_update: update existing scm activity or not
        :return: None
        """

        with requests.Session() as session:

            session.headers.update({'Content-Type': 'application/json'})

            'Update issues'

            jira_rest_url = self.jira_cloud_scm_url

            if type(scm_activity) is ScmActivity:

                for e_key in issue_keys:

                    'print "[Info] processing Issue update -> key:{0}".format(e_key)'

                    scm_activity.issueKey = e_key
                    scm_activity.changeDate = self.get_jira_utc_datetime(scm_activity.changeDate)

                    if change_update == 1:
                        scm_activity.changeUpdate = "true"

                    'print ( dumps(scm_activity.__dict__, indent=4) )'

                    response = session.post(jira_rest_url, json=scm_activity.__dict__)

                    if response.status_code == 201:
                        sys.stdout.write( "{0}\n".format(response.json().get("message")) )
                    elif response.status_code == 403:
                        sys.stderr.write( "[Error] Invalid JIRA token - 403\n" )
                    elif response.status_code == 400:
                        sys.stderr.write( "{0}\n".format(response.json().get("message")) )
                    else:
                        sys.stderr.write( "{0}\n".format(response.text) )

            else:
                sys.stderr.write( "[Error] Type is not Scm Activity type.\n" )

    def get_jira_utc_datetime(self, raw_datetime):
        """
        get_jira_datetime(). gives jira application date time for the given date time
        :param raw_datetime: change date time
        :return: str
        """

        jira_timezone = pytz.timezone("UTC")
        hook_timezone = tzlocal.get_localzone()
        date_fmt = "%Y-%m-%d %H:%M:%S"
        hook_parse_dt_str =  datetime.strptime(raw_datetime, date_fmt)
        hook_local_dt = hook_timezone.localize(hook_parse_dt_str)
        jira_local_dt = hook_local_dt.astimezone(jira_timezone)

        return jira_local_dt.strftime(date_fmt)

    def pattern_validate(self, message):
        """
        pattern_validate(). pattern matches the given message content
        :param message: change message
        :return: list
        """

        issue_patterns = self.config.get("jira.issue.patterns")
        issue_keys = {}

        for pattern in issue_patterns:
            for match in re.finditer(pattern, message, re.IGNORECASE):
                'logging("{0}".format(match.group()))'

                key = re.sub("[\[()\]]|JIRA\s+", "", match.group(), flags=re.IGNORECASE)

                arg = re.search("fix|resolve", key, re.IGNORECASE)

                if arg:
                    confirm_key = re.sub("fix|resolve", "", key, flags=re.IGNORECASE)
                    issue_keys[confirm_key.strip()] = arg.group().strip()
                else:
                    'print "[Info] key: {0} -> arg: none".format(key))'
                    issue_keys[key.strip()] = None

        return issue_keys

    def command_output(self, cmd):
        """
        command_output(). It get command out put for give cmd
        :param cmd: command
        :return: None
        """

        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)

        (output, err) = p.communicate()

        status = p.wait()

        if status == 0:
            return output.decode('ascii').strip()
        else:
            sys.stderr.write( "{0}\n".format(err) )
            sys.exit(1)
