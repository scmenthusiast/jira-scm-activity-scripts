#### Pre-requisites
Install Python perforce module
```
pip install p4python
```

```
$ python

>>> from P4 import P4,P4Exception
>>> quit()

$
```


#### Trigger Deployment

```
$ p4 triggers

Triggers:
	jira_post_commit change-commit //depot/... "python p4_post_commit.py -name it -change %changelist%"

```
Note:
* If possible add perforce instance name with option -name (e.g. -name bluetooth)

#### Customize triggers configurations

1) Define username and password to connect perforce (default it connects using p4 tickets at HOME/.p4tickets) e.g.

`"perforce.username" : "mamatha"`

`"perforce.password" : "secret"`

2) To run triggers against different configuration file (default is p4_cfg.json)
use -config option e.g.

`python p4_post_commit.py -config modem_p4_cfg.json -change %changelist%`

