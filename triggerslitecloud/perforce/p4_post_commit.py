#!/usr/bin/env python

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from jira_commit_lib import JiraCommitHook
from jira_commit_lib import ScmActivity
from json import load, loads, dumps
from P4 import P4,P4Exception
from datetime import datetime
import argparse
import os


class PerforcePostCommitHook(object):

    def __init__(self, config):
        """
        __init__(). prepares or initiates configuration file
        :return: None
        """

        if not config: config = "p4_cfg.json" # default config file

        self.config_file = os.path.join(os.path.dirname(__file__), config)

        if self.config_file and os.path.exists(self.config_file):
            try:
                config = load(open(self.config_file))

                self.p4_server_port = config.get("perforce.server.port")
                self.p4_server_web = config.get("perforce.server.web")
                self.p4_username = config.get("perforce.username")
                self.p4_password = config.get("perforce.password")

                self.jch = JiraCommitHook(config)

            except ValueError as e:
                print (e)
                exit(1)
        else:
            print ("Config file ({0}) not exists!".format(config))
            exit(1)

    def get_p4_change(self, change_id):
        """
        run(). executes the jira update for the given change-set
        :param change_id: Perforce change list number
        :return: object
        """

        p4 = P4()
        p4.port = str(self.p4_server_port)
        p4.ticket_file = os.path.join(os.path.expanduser("~"),".p4tickets")
        if self.p4_username and self.p4_password:
            p4.user = str(self.p4_username)
            p4.password = str(self.p4_password)

        try:
            p4.connect()

            p4_desc = p4.run( "describe", change_id )

            if len(p4_desc) == 1:

                '''for key in p4_desc[0]:
                    print key, "=", p4_desc[0][key]'''

                changeset = ScmActivity()
                changeset.changeAuthor = p4_desc[0]['user']
                changeset.changeId = p4_desc[0]['change']                

                change_datetime = datetime.fromtimestamp(
                    int(p4_desc[0]['time'])
                ).strftime('%Y-%m-%d %H:%M:%S')

                changeset.changeDate = change_datetime
                changeset.changeMessage = p4_desc[0]['desc']
                changeset.changeStatus = p4_desc[0]['status']

                if p4_desc[0].get('depotFile'):
                    change_files = []
                    for index in range(len(p4_desc[0]['depotFile'])):
                        change_file = {
                            "fileName" : p4_desc[0]['depotFile'][index],
                            "fileAction" : p4_desc[0]['action'][index],
                            "fileVersion" : p4_desc[0]['rev'][index]
                        }
                        change_files.append(change_file)

                    changeset.changeFiles = change_files

                'print dumps(changeset.__dict__, indent=4)'

                return changeset

            p4.disconnect()
        except P4Exception:
          for e in p4.errors:
              print(e)

    def run(self, args):
        """
        run(). executes the jira update for the given change-set
        :param args: Perforce hook args list
        :return: None
        """

        p4_changeset = self.get_p4_change(args.change)

        if p4_changeset:
            'print dumps(p4_changeset.__dict__, indent=4)'
            matched_issue_keys = self.jch.pattern_validate(p4_changeset.changeMessage)

            p4_changeset.changeLink = self.p4_server_web.format(args.change)

            # Preferred format: ChangeType_Instance Name e.g. p4_ccx
            if args.name:
                p4_changeset.changeType = "{0}_{1}".format('perforce', args.name)
            else:
                p4_changeset.changeType = 'perforce'

            if len(matched_issue_keys.keys()) > 0:
                self.jch.jira_update(p4_changeset, matched_issue_keys.keys())
                'self.jch.jira_update(p4_change, matched_issue_keys.keys(), 1)' # to update existing activity


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """

    parser = argparse.ArgumentParser(description='Perforce Post Commit Trigger')
    parser.add_argument("-config", help='Trigger Config file')
    parser.add_argument("-name", help='Perforce Instance Name')
    parser.add_argument("-change", help='Perforce Change list number', required=True)

    args = parser.parse_args()

    g = PerforcePostCommitHook(args.config)
    g.run(args)


if __name__ == '__main__':
    main()
