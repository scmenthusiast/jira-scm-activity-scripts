#### Trigger Deployment
Go to gerrit install hooks directory, Copy hook scripts and create soft links for below hooks.
```
$ cd /export/gerrit_site/hooks

$ ln -s gerrit_hook.py patchset-added
$ ln -s gerrit_hook.py comment-added

```

#### Customize triggers configurations

1) Define gerrit instance name (Recommeded) e.g.

`"gerrit.name" : "modem"`

2) Define ssh path if custom (default is ssh)

`"gerrit.ssh.path" : "/usr/bin/ssh"`

3) To run triggers against different configuration file (default is gerrit_cfg.json)
edit gerrit_generic_hook.py > navigate to main() function > define config file name inside below constructor

`g = GerritGenericHook("modem_cfg.json")`
