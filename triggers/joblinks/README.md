#### Posting External Job Links Demo
Download the scripts and execute as follows

`$ python joblink_add.py -issueKey SD-1 -changeId 104 -changeType perforce -jobName "Test_Build#79" -jobStatus "SUCCESS" -jobLink "http://jenkins/job/test_build/79"`

`{"result":1,"message":"[Info] SD-1 > 104 > Test_Build#79 joblink is added."}`

`$`

#### Customize script configurations

1) Enable to send email (default is false) e.g.

`"jira.notify.email" : "true"`

2) Define jira username to notify as custom user (default is commit user) e.g.

`"jira.notify.as" : "scmcommit"`

3) To run triggers against different configuration file (default is default_cfg.json)
edit joblink_add.py > navigate to main() function > define config file name inside below constructor

`g = ScmActivityJobLink("modem_cfg.json")`


**To generate auth token**
```
$ python
>>> import base64
>>> base64 .b64encode("jirauser:password")
'cHJlY29tbWl0Omxldf1l****'
>>>
```