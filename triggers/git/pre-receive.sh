#!/usr/bin/env bash

# read std hook inputs
read oldrev newrev refname

# define required vars
python_path=python
git_hook=`dirname $0`/jira_hooks/git_pre_receive.py

# execute
${python_path} ${git_hook} --config "git_cfg.json" --oldrev ${oldrev} --newrev ${newrev} --refname ${refname} || exit 1

exit 0