#!/usr/bin/env python

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from jira_commit_lib import JiraCommitHook
from json import load, loads, dumps
from P4 import P4,P4Exception
import argparse
import os


class PerforcePreCommitHook(object):

    def __init__(self, config):
        """
        __init__(). prepares or initiates configuration file
        :return: None
        """

        if not config: config = "p4_cfg.json" # default config file

        self.config_file = os.path.join(os.path.dirname(__file__), config)

        if self.config_file and os.path.exists(self.config_file):
            try:
                config = load(open(self.config_file))

                self.p4_server_port = config.get("perforce.server.port")
                self.p4_server_web = config.get("perforce.server.web")
                self.p4_username = config.get("perforce.username")
                self.p4_password = config.get("perforce.password")

                self.jch = JiraCommitHook(config)

            except ValueError, e:
                print (e)
                exit(1)
        else:
            print ("Config file ({0}) not exists!".format(config))
            exit(1)

    def get_p4_change_message(self, change):
        """
        run(). executes the jira update for the given change-set
        :param change: Perforce change list number
        :return: object
        """

        p4 = P4()
        p4.port = str(self.p4_server_port)
        p4.ticket_file = os.path.join(os.path.expanduser("~"),".p4tickets")
        if self.p4_username and self.p4_password:
            p4.user = str(self.p4_username)
            p4.password = str(self.p4_password)

        try:
            p4.connect()

            p4_desc = p4.run( "describe", change )

            if len(p4_desc) == 1:

                '''for key in p4_desc[0]:
                    print key, "=", p4_desc[0][key]'''

                return p4_desc[0]['desc']

            p4.disconnect()
        except P4Exception:
          for e in p4.errors:
              print e



    def run(self, change):
        """
        run(). executes the jira update for the given change-set
        :param change: Perforce change list number
        :return: None
        """

        p4_change_message = self.get_p4_change_message(change)

        if p4_change_message:

            matched_issue_keys = self.jch.pattern_validate(p4_change_message)

            if len(matched_issue_keys) != 0:
                self.jch.jira_pre_validate(matched_issue_keys.keys())
            else:
                print "[Error] *** Required atleast one JIRA Issue ID. e.g. [JIRA DUMMY-5]."
                exit(1)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """

    parser = argparse.ArgumentParser(description='Perforce Pre Commit Trigger')
    parser.add_argument("-config", help='Trigger Config file')
    parser.add_argument("-change", help='Perforce Change list number', required=True)

    args = parser.parse_args()

    g = PerforcePreCommitHook(args.config)
    g.run(args.change)


if __name__ == '__main__':
    main()
