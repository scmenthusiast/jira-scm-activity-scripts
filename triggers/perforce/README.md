#### Pre-requisites
Install Python perforce module
```
pip install p4python
```

```
$ python

>>> from P4 import P4,P4Exception
>>> quit()

$
```


#### Trigger Deployment

```
$ p4 triggers

Triggers:
	jira_post_commit change-commit //depot/... "python p4_post_commit.py -name it -change %changelist%"
	jira_pre_commit change-submit //depot/... "python p4_pre_commit.py -change %changelist%"

```
Note:
* Add JIRA Pre-commit trigger only if you like to enforce to specify JIRA issue key must
* If possible add perforce instance name with option -name (e.g. -name bluetooth)

#### Customize triggers configurations

1) Enable to send email (default is false) e.g.

`"jira.notify.email" : "true"`

2) Define jira username to notify as custom user (default is commit user) e.g.

`"jira.notify.as" : "scmcommit"`

3) Define username and password to connect perforce (default it connects using p4 tickets at HOME/.p4tickets) e.g.

`"perforce.username" : "mamatha"`

`"perforce.password" : "secret"`

4) To run triggers against different configuration file (default is p4_cfg.json)
use -config option e.g.

`python p4_post_commit.py -config modem_p4_cfg.json -change %changelist%`


**To generate auth token**
```
$ python
>>> import base64
>>> base64 .b64encode("jirauser:password")
'cHJlY29tbWl0Omxldf1l****'
>>>
```
