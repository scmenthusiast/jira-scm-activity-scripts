#### Trigger Deployment
Copy hook scripts to subversion repository hooks/jirahooks directory and create soft links
```
$ ln -s jirahooks/post-commit.sh post-commit
$ ln -s jirahooks/pre-commit.sh pre-commit
```
Note: Add JIRA Pre-commit trigger only if you like to enforce to specify JIRA issue key must.

#### Customize triggers configurations

1) Enable to send email (default is false) e.g.

`"jira.notify.email" : "true"`

2) Define jira username to notify as custom user (default is commit user) e.g.

`"jira.notify.as" : "precommit"`

3) To run hooks against different configuration file (default is svn_cfg.json)
edit pre/post-commit.sh > specify --config option

`$svnhook --config "modem_cfg.json" ...`

4) To process branches and tag names (default is false) e.g.

`"svn.branch.tags.processing" : true`



**To generate jira auth token**
```
$ python3
>>> import base64
>>> base64.b64encode(b"jirauser:password")
'cHJlY29tbWl0Omxldf1l****'
>>>
```
