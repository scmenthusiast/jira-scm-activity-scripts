#### Trigger Deployment
Copy hook scripts to git repository (hooks/jirahooks) directory and create soft links
```
$ cd /export/repos/git_repo/hooks

### Post receive hook (Server-side)
$ ln -s jira_hooks/post-receive.sh post-receive

### To enforce JIRA issue key (Server-side)
$ ln -s jira_hooks/update.sh update
  OR
$ ln -s jira_hooks/pre-receive.sh pre-receive

### To enforce JIRA issue key (Client-side)
$ ln -s jira_hooks/commit-msg.sh commit-msg

### To enforce JIRA issue key (Windows Client-side)
C:\PATH\.git\hooks> copy jira_hooks\commit-msg.sh commit-msg

```

#### Customize triggers configurations

1) Enable to send email (default is false) e.g.

`"jira.notify.email" : "true"`

2) Define jira username to notify as custom user (default is commit user) e.g.

`"jira.notify.as" : "scmcommit"`

3) To run triggers against different configuration file (default is git_cfg.json)
edit post-receive.sh > define the option --config

`-config "modem_cfg.json"`


**To generate jira auth token**
```
$ python
>>> import base64
>>> base64 .b64encode("jirauser:password")
'cHJlY29tbWl0Omxldf1l****'
>>>
```
