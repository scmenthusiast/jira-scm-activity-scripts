#!/usr/bin/env python

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from json import load, loads, dumps
import requests
import os

"""
    This python demo script will add jobs link to the given change and issue keys on JIRA.
    Parameters:
        - Configuration file name (Optional - default is coco_cfg.json)
"""


class ScmActivityJobLink(object):

    def __init__(self, config='default_cfg.json'):
        """
        __init__(). prepares configuration
        :param config: config file
        :return: None
        """

        self.config_file = os.path.join(os.path.dirname(__file__), config)

        if os.path.exists( self.config_file ):
            try:
                config = load(open(self.config_file))

                self.jira_server_url = config.get("jira.server.url")
                self.jira_auth_token = config.get("jira.auth.token")
                self.jira_notify = config.get("jira.notify.email")
                self.jira_notify_as = config.get("jira.notify.as")

            except ValueError, e:
                print (e)
                exit(1)
        else:
            print("Error: Config file not exists!")
            exit(1)

    def jira_job_link_update(self, job_set):
        """
        jira_update(). It updates change-set details for give issue keys on JIRA
        :param job_set: job set dictionary object
        :return: None
        """

        with requests.Session() as session:

            session.headers.update({'Content-Type': 'application/json'})
            session.headers.update({"Authorization": "Basic {0}".format(self.jira_auth_token)})

            jira_rest_url = self.jira_server_url + "/rest/scmchangeset/1.0/changeset/joblink"

            response = session.post(jira_rest_url, json=job_set)

            if response.status_code == 200:
                message_object = response.json()
                print "Info: {0}".format(message_object.get("message"))
            else:
                print(response.text)

    def run(self, args):
        """
        run(). executes the jira job link update for the given args
        :param args: job link args
        :return: None
        """

        job_set = {
            "changeType" : args.changeType,
            "changeId" : args.changeId,
            "issueKey" : args.issueKey,
            "jobName" : args.jobName,
            "jobLink" : args.jobLink,
            "jobStatus" : args.jobStatus
        }

        if self.jira_notify and self.jira_notify == "true":
            job_set.update(notifyEmail=self.jira_notify)

        if self.jira_notify_as and self.jira_notify_as != "":
            job_set.update(notifyAs=self.jira_notify_as)

        if args.jobUpdate:
            job_set.update(jobUpdate=args.jobUpdate)

        'print dumps(job_set, indent=4)'

        self.jira_job_link_update(job_set)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """

    import argparse

    parser = argparse.ArgumentParser(description='SCM Activity Jobs Add Demo Execution Script')
    parser.add_argument("-issueKey", help='Required Jira Issue Key', required=True)
    parser.add_argument("-changeId", help='Required Change Id', required=True)
    parser.add_argument("-changeType", help='Required Change Type', required=True)
    parser.add_argument("-jobName", help='Required Job Name', required=True)
    parser.add_argument("-jobStatus", help='Required Job Status', required=True)
    parser.add_argument("-jobLink", help='Required Job Link', required=True)
    parser.add_argument("-jobUpdate", help='Optional to update an existing Job', action='store_true')

    args = parser.parse_args()

    # g = ScmActivityJobLink("modem_cfg.json") # for custom configuration file
    g = ScmActivityJobLink() # this uses default config
    g.run(args)


if __name__ == '__main__':
    main()
